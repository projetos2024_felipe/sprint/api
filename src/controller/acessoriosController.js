const connect = require("../db/connect");

module.exports = class AcessorioController {
    // Método para criar um novo acessório
    static async createAcessorio(req, res) {
        // Extrai os dados do corpo da requisição
        const { nome, descricao, preco, estoque_disponivel } = req.body;

        try {
            // Verifica se todos os campos obrigatórios estão presentes
            if (!nome || !descricao || !preco || !estoque_disponivel) {
                throw new Error("Por favor, preencha todos os campos corretamente");
            }

            // Query SQL para inserir um novo acessório na tabela 'acessorios'
            const query = `INSERT INTO acessorios (nome, descricao, preco, estoque_disponivel) VALUES (?, ?, ?, ?)`;
            const values = [nome, descricao, preco, estoque_disponivel];

            // Executa a consulta no banco de dados
            connect.query(query, values, function (err, results) {
                if (err) {
                    console.error("Erro ao criar acessorio:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de sucesso se o acessório for criado com sucesso
                return res.status(201).json({ message: "Acessorio criado com sucesso." });
            });
        } catch (error) {
            console.error("Erro ao criar acessorio:", error.message);
            // Retorna um erro se houver algum problema ao criar o acessório
            return res.status(400).json({ message: error.message });
        }
    }

    // Método para atualizar um acessório existente
    static async updateAcessorios(req, res) {
        // Obtém o ID do acessório a ser atualizado a partir dos parâmetros da requisição
        const productId = req.params.id;
        // Extrai os novos dados do acessório do corpo da requisição
        const { nome, descricao, preco, estoque_disponivel } = req.body;

        try {
            // Verifica se o ID do produto é válido e se todos os campos obrigatórios estão presentes
            if (!productId || isNaN(productId) || parseInt(productId) <= 0 || !nome || !preco || !estoque_disponivel) {
                throw new Error("ID de acessório inválido ou campos incompletos");
            }

            // Query SQL para atualizar os campos na tabela 'acessorios'
            const query = `UPDATE Acessorios SET Nome = ?, Descricao = ?, Preco = ?, Estoque_disponivel = ? WHERE ID_produto = ?`;
            const values = [nome, descricao, preco, estoque_disponivel, productId];

            // Executa a consulta no banco de dados
            connect.query(query, values, function (err, results) {
                if (err) {
                    console.error("Erro ao atualizar acessório:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }

                // Verifica se houve algum registro atualizado
                if (results.affectedRows === 0) {
                    return res.status(404).json({ message: "Acessório não encontrado" });
                }

                // Retorna uma mensagem de sucesso se o acessório for atualizado com sucesso
                return res.status(200).json({ message: "Acessório atualizado" });
            });
        } catch (error) {
            console.error("Erro ao atualizar acessório:", error.message);
            // Retorna um erro se houver algum problema ao atualizar o acessório
            return res.status(400).json({ message: error.message });
        }
    }

    // Método para obter todos os acessórios
    static async getAcessorios(req, res) {
        try {
            // Query SQL para selecionar todos os acessórios da tabela 'acessorios'
            const query = "SELECT * FROM acessorios";
            
            // Executa a consulta no banco de dados
            connect.query(query, function (err, results) {
                if (err) {
                    console.error("Erro ao obter informações do acessorio:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de erro se nenhum acessório for encontrado
                if (results.length === 0) {
                    return res.status(404).json({ message: "Acessorio não encontrado" });
                }
                // Retorna os acessórios encontrados se a consulta for bem-sucedida
                return res.status(200).json(results);
            });
        } catch (error) {
            console.error("Erro ao obter informações do acessorio:", error.message);
            // Retorna um erro se houver algum problema ao obter os acessórios
            return res.status(500).json({ message: "Erro interno do servidor" });
        }
    }
    
    // Método para obter um acessório por ID
    static async getAcessoriosID(req, res) {
        // Obtém o ID do acessório a ser obtido a partir dos parâmetros da requisição
        const { id } = req.params;

        try {
            // Query SQL para selecionar um acessório específico com base no ID
            const query = `SELECT * FROM acessorios WHERE id_produto = ?`;
            const values = [id];

            // Executa a consulta no banco de dados
            connect.query(query, values, function (err, results) {
                if (err) {
                    console.error("Erro ao obter informações do acessorio:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de erro se o acessório não for encontrado
                if (results.length === 0) {
                    return res.status(404).json({ message: "Acessorio não encontrado" });
                }
                // Retorna o acessório encontrado se a consulta for bem-sucedida
                return res.status(200).json(results[0]);
            });
        } catch (error) {
            console.error("Erro ao obter informações do acessorio:", error.message);
            // Retorna um erro se houver algum problema ao obter o acessório
            return res.status(500).json({ message: "Erro interno do servidor" });
        }
    }

    // Método para excluir um acessório por ID
    static async deleteAcessoriosPorId(req, res) {
        // Obtém o ID do acessório a ser excluído a partir dos parâmetros da requisição
        const { id } = req.params;

        try {
            // Verifica se o ID do acessório é válido
            if (!id || isNaN(id) || parseInt(id) <= 0) {
                throw new Error("ID de acessorio inválido");
            }

            // Query SQL para excluir um acessório específico com base no ID
            const query = `DELETE FROM acessorios WHERE id_produto = ?`;
            const values = [id];

            // Executa a consulta no banco de dados
            connect.query(query, values, function (err, results) {
                if (err) {
                    console.error("Erro ao excluir acessorio:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de sucesso se o acessório for excluído com sucesso
                return res.status(200).json({ message: `Acessorio removido com ID ${id}` });
            });
        } catch (error) {
            console.error("Erro ao excluir acessorio:", error.message);
            // Retorna um erro se houver algum problema ao excluir o acessório
            return res.status(400).json({ message: error.message });
        }
    }
};
