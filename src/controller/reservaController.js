const connect = require("../db/connect");

module.exports = class reservaController {
  // Método para criar uma reserva
  static async createReserva(req, res) {
    // Extrai os dados da requisição
    const { ID_produto, ID_cliente, Data_da_reserva, Data_de_retirada, Data_de_devolucao, Status_da_reserva } = req.body;

    // Verifica se todos os campos obrigatórios estão presentes
    if (!ID_produto || !ID_cliente || !Data_da_reserva || !Data_de_retirada || !Data_de_devolucao || !Status_da_reserva) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Monta a consulta SQL para inserir uma nova reserva
    const query = `INSERT INTO reserva (ID_produto, ID_cliente, Data_da_reserva, Data_de_retirada, Data_de_devolucao, Status_da_reserva) VALUES (?, ?, ?, ?, ?, ?)`;
    const values = [ID_produto, ID_cliente, Data_da_reserva, Data_de_retirada, Data_de_devolucao, Status_da_reserva];

    try {
      // Executa a consulta SQL para inserir a reserva
      connect.query(query, values, function (err) {
        if (err) {
          // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }
        // Retorna uma mensagem de sucesso se a reserva for criada com sucesso
        console.log("Reserva cadastrada com sucesso");
        return res.status(201).json({ message: "Reserva cadastrada com sucesso" });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para obter todas as reservas
  static async getAllReservas(req, res) {
    // Monta a consulta SQL para obter todas as reservas
    const query = `SELECT * FROM reserva`;

    try {
      // Executa a consulta SQL para obter todas as reservas
      connect.query(query, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Retorna os resultados se as reservas forem encontradas com sucesso
        return res.status(200).json({ message: "Obtendo todas as reservas", reservas: results });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para obter uma reserva por ID
  static async getReservaPorId(req, res) {
    // Extrai o ID da reserva da requisição
    const reservaId = req.params.id;
    const query = `SELECT * FROM reserva WHERE ID_reserva = ?`;
    const values = [reservaId];

    try {
      // Executa a consulta SQL para obter a reserva com base no ID
      connect.query(query, values, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verifica se alguma reserva foi encontrada
        if (results.length === 0) {
          // Retorna um erro se nenhuma reserva for encontrada
          return res.status(404).json({ error: "Reserva não encontrada" });
        }

        // Retorna os resultados se a reserva for encontrada com sucesso
        return res.status(200).json({ message: "Obtendo reserva com ID: " + reservaId, reserva: results[0] });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para atualizar uma reserva
  static async updateReserva(req, res) {
    // Extrai o ID da reserva da requisição e os novos dados da reserva
    const reservaId = req.params.id;
    const { ID_produto, ID_cliente, Data_da_reserva, Data_de_retirada, Data_de_devolucao, Status_da_reserva } = req.body;

    // Verifica se todos os campos obrigatórios estão presentes
    if (!ID_produto || !ID_cliente || !Data_da_reserva || !Data_de_retirada || !Data_de_devolucao || !Status_da_reserva) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Monta a consulta SQL para atualizar a reserva com base no ID
    const query = `UPDATE reserva SET ID_produto = ?, ID_cliente = ?, Data_da_reserva = ?, Data_de_retirada = ?, Data_de_devolucao = ?, Status_da_reserva = ? WHERE ID_reserva = ?`;
    const values = [ID_produto, ID_cliente, Data_da_reserva, Data_de_retirada, Data_de_devolucao, Status_da_reserva, reservaId];

    try {
      // Executa a consulta SQL para atualizar a reserva
      connect.query(query, values, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verifica se a reserva foi atualizada com sucesso
        if (results.affectedRows === 0) {
          // Retorna um erro se a reserva não for encontrada
          return res.status(404).json({ error: "Reserva não encontrada" });
        }

        // Retorna uma mensagem de sucesso se a reserva for atualizada com sucesso
        return res.status(200).json({ message: "Reserva atualizada com ID: " + reservaId });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para excluir uma reserva por ID
  static async deleteReserva(req, res) {
    // Extrai o ID da reserva da requisição
    const reservaId = req.params.id;
    const query = `DELETE FROM reserva WHERE ID_reserva = ?`;
    const values = [reservaId];

    try {
      // Executa a consulta SQL para excluir a reserva com base no ID
      connect.query(query, values, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verifica se a reserva foi excluída com sucesso
        if (results.affectedRows === 0) {
          // Retorna um erro se a reserva não for encontrada
          return res.status(404).json({ error: "Reserva não encontrada" });
        }

        // Retorna uma mensagem de sucesso se a reserva for excluída com sucesso
        return res.status(200).json({ message: "Reserva excluída com ID: " + reservaId });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};
