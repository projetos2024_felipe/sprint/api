const connect = require("../db/connect");

module.exports = class EnderecoController {
    // Método para criar um novo endereço
    static async createEndereco(req, res) {
        const { cep, rua, numero, complemento, referencia, id_cliente } = req.body;

        try {
            // Verifica se todos os campos obrigatórios estão preenchidos
            if (!cep || !rua || !numero || !id_cliente) {
                throw new Error("Por favor, preencha todos os campos obrigatórios");
            }

            // Monta a consulta SQL para inserir um novo endereço
            const query = `INSERT INTO Endereco (CEP, Rua, Numero, Complemento, Referencia, ID_cliente) VALUES (?, ?, ?, ?, ?, ?)`;
            const values = [cep, rua, numero, complemento, referencia, id_cliente];

            // Executa a consulta SQL para inserir o novo endereço
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao criar endereço:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de sucesso se o endereço for criado com sucesso
                return res.status(201).json({ message: "Endereço criado com sucesso." });
            });
        } catch (error) {
            // Retorna um erro de requisição inválida se houver um problema durante a validação dos dados
            console.error("Erro ao criar endereço:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    // Método para atualizar um endereço
    static async updateEndereco(req, res) {
        const cepId = req.params.id;
        const { cep, rua, numero, complemento, referencia, id_cliente } = req.body;

        try {
            // Verifica se o ID do CEP é válido e se todos os campos obrigatórios estão preenchidos
            if (!cepId || isNaN(cepId) || parseInt(cepId) <= 0 || !rua || !numero || !id_cliente) {
                throw new Error("ID de CEP inválido ou campos incompletos");
            }

            // Monta a consulta SQL para atualizar o endereço
            const query = `UPDATE Endereco SET CEP = ?, Rua = ?, Numero = ?, Complemento = ?, Referencia = ?, ID_cliente = ? WHERE id_endereco = ?`;
            const values = [cep, rua, numero, complemento, referencia, id_cliente, cepId];

            // Executa a consulta SQL para atualizar o endereço
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao atualizar endereço:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }

                // Verifica se o endereço foi atualizado com sucesso
                if (results.affectedRows === 0) {
                    // Retorna um erro se o endereço não for encontrado
                    return res.status(404).json({ message: "Endereço não encontrado" });
                }

                // Retorna uma mensagem de sucesso se o endereço for atualizado com sucesso
                return res.status(200).json({ message: "Endereço atualizado" });
            });
        } catch (error) {
            // Retorna um erro de requisição inválida se houver um problema durante a validação dos dados
            console.error("Erro ao atualizar endereço:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    // Método para obter todos os endereços
    static async getEnderecos(req, res) {
        try {
            // Monta a consulta SQL para obter todos os endereços
            const query = "SELECT * FROM Endereco";
            
            // Executa a consulta SQL para obter todos os endereços
            connect.query(query, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao obter informações do endereço:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Verifica se algum endereço foi encontrado
                if (results.length === 0) {
                    // Retorna um erro se nenhum endereço for encontrado
                    return res.status(404).json({ message: "Endereço não encontrado" });
                }
                // Retorna os resultados se os endereços forem encontrados com sucesso
                return res.status(200).json(results);
            });
        } catch (error) {
            // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
            console.error("Erro ao obter informações do endereço:", error.message);
            return res.status(500).json({ message: "Erro interno do servidor" });
        }
    }

    // Método para obter um endereço por ID
    static async getEnderecoByID(req, res) {
        const { id } = req.params;
    
        try {
            // Monta a consulta SQL para obter o endereço com base no CEP
            const query = `SELECT * FROM Endereco WHERE id_endereco = ?`;
            const values = [id];
    
            // Executa a consulta SQL para obter o endereço com base no CEP
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao obter informações do endereço:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Verifica se algum endereço foi encontrado
                if (results.length === 0) {
                    // Retorna um erro se nenhum endereço for encontrado
                    return res.status(404).json({ message: "Endereço não encontrado" });
                }
                // Retorna os resultados se os endereços forem encontrados com sucesso
                return res.status(200).json(results[0]);
            });
        } catch (error) {
            // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
            console.error("Erro ao obter informações do endereço:", error.message);
            return res.status(500).json({ message: "Erro interno do servidor" });
        }
    }
    

    // Método para excluir um endereço por ID
    static async deleteEnderecoByID(req, res) {
        const { id } = req.params;

        try {
            // Verifica se o ID do endereço é válido
            if (!id || isNaN(id) || parseInt(id) <= 0) {
                throw new Error("ID de endereço inválido");
            }

            // Monta a consulta SQL para excluir o endereço com base no ID
            const query = `DELETE FROM Endereco WHERE id_endereco = ?`;
            const values = [id];

            // Executa a consulta SQL para excluir o endereço com base no ID
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao excluir endereço:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de sucesso se o endereço for excluído com sucesso
                return res.status(200).json({ message: `Endereço removido com o  ${id}` });
            });
        } catch (error) {
            // Retorna um erro de requisição inválida se houver um problema durante a validação dos dados
            console.error("Erro ao excluir endereço:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }
};

