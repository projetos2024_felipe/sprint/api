const connect = require("../db/connect");

module.exports = class clienteController {
  // Método para criar um novo cliente
  static async createCliente(req, res) {
    const { nome, telefone, email, senha } = req.body;

    // Verifica se todos os campos estão preenchidos
    if (!nome || !telefone || !email || !senha) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }

    // Verifica se o Telefone é numérico e tem exatamente 11 dígitos
    else if (isNaN(telefone) || telefone.length !== 11) {
      return res.status(400).json({ error: "Telefone inválido. Deve conter exatamente 11 dígitos numéricos" });
    }

    // Verifica se o email contém o caractere @
    else if (!email.includes("@")) {
      return res.status(400).json({ error: "Email inválido. Deve conter @" });
    }

    //Caso esteja tudo certo, cadastra o usuário
    else {
      // Monta a consulta SQL para inserir um novo cliente
      const query = `INSERT INTO cliente (nome, telefone, email, senha) VALUES ( 
        '${nome}', 
        '${telefone}', 
        '${email}', 
        '${senha}'
      )`;

      try {
        // Executa a consulta SQL para inserir o cliente
        connect.query(query, function (err) {
          if (err) {
            // Verifica se é um erro de chave primária duplicada (telefone já cadastrado)
            if (err.code === 'ER_DUP_ENTRY') { 
              return res.status(400).json({ error: "Telefone já cadastrado" });
            } else {
              // Retorna um erro interno do servidor caso contrário
              console.error(err);
              return res.status(500).json({ error: "Erro interno do servidor" });
            }
          }
          // Retorna uma mensagem de sucesso se o cliente for cadastrado com sucesso
          return res.status(201).json({ message: "Usuário criado com sucesso" });
        });
      } catch (error) {
        // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
        console.error("Erro ao executar a consulta:", error);
        res.status(500).json({ error: "Erro interno do servidor" });
      }
    } 
  }

  // Método para fazer login de um cliente
  static async postLogin(req, res) {
    const { email, senha } = req.body;

    // Verifica se o email e a senha foram fornecidos
    if (!email || !senha) {
      return res.status(400).json({ error: "Email e senha são obrigatórios" });
    }

    // Monta a consulta SQL para selecionar o cliente com base no email e senha fornecidos
    const query = `SELECT * FROM cliente WHERE email = '${email}' AND senha = '${senha}'`;

    try {
      // Executa a consulta SQL para verificar as credenciais do cliente
      connect.query(query, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verifica se algum cliente foi encontrado com as credenciais fornecidas
        if (results.length === 0) {
          // Retorna um erro de credenciais inválidas se nenhum cliente for encontrado
          return res.status(401).json({ error: "Credenciais inválidas" });
        }

        // Retorna uma mensagem de sucesso e os dados do cliente se o login for bem-sucedido
        return res.status(200).json({ message: "Login realizado com sucesso", user: results[0] });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para obter todos os clientes
  static async getAllClientes(req, res) {
    // Monta a consulta SQL para selecionar todos os clientes
    const query = `SELECT * FROM cliente`;

    try {
      // Executa a consulta SQL para obter todos os clientes
      connect.query(query, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Retorna uma mensagem de sucesso e os dados de todos os clientes
        return res.status(200).json({ message: "Obtendo todos os usuários", users: results });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para obter um cliente por ID
  static async getClientesPorId(req, res) {
    const userId = req.params.id;
    // Monta a consulta SQL para selecionar um cliente com base no ID
    const query = `SELECT * FROM cliente WHERE telefone = ?`;
    const values = [userId];

    try {
      // Executa a consulta SQL para obter o cliente com o ID fornecido
      connect.query(query, values, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verifica se algum cliente foi encontrado com o ID fornecido
        if (results.length === 0) {
          // Retorna um erro se nenhum cliente for encontrado com o ID fornecido
          return res.status(404).json({ error: "Usuário não encontrado" });
        }

        // Retorna uma mensagem de sucesso e os dados do cliente com o ID fornecido
        return res.status(200).json({ message: "Obtendo usuário com ID: " + userId, user: results[0] });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }

  // Método para atualizar um cliente
  static async updateCliente(req, res) {
    const userId = req.params.id;
    const { nome, telefone, email, senha } = req.body;
    // Verifica se todos os campos estão preenchidos
    if (!nome || !telefone || !email || !senha) {
      return res.status(400).json({ error: "Todos os campos devem ser preenchidos" });
    }
    else
    {
      // Monta a consulta SQL para atualizar os dados do cliente
      const query = `UPDATE cliente SET telefone = ?, email = ?, senha = ?, nome = ? WHERE telefone = ?`;
      const values = [telefone, email, senha, nome, userId];

      try {
        // Executa a consulta SQL para atualizar os dados do cliente
        connect.query(query, values, function (err, results) {
          if (err) {
            // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
            console.error(err);
            return res.status(500).json({ error: "Erro interno do servidor" });
          }

          // Verifica se algum cliente foi atualizado
          if (results.affectedRows === 0) {
            // Retorna um erro se nenhum cliente for encontrado com o ID fornecido
            return res.status(404).json({ error: "Usuário não encontrado" });
          }

          // Retorna uma mensagem de sucesso se o cliente for atualizado com sucesso
          return res.status(200).json({ message: "Usuário atualizado com ID: " + userId });
        });
      } catch (error) {
        // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
        console.error("Erro ao executar a consulta:", error);
        return res.status(500).json({ error: "Erro interno do servidor" });
      }
    }
  }

  // Método para excluir um cliente
  static async deleteCliente(req, res) {
    const userId = req.params.id;
    // Monta a consulta SQL para excluir o cliente com base no ID
    const query = `DELETE FROM cliente WHERE telefone = ?`;
    const values = [userId];

    try {
      // Executa a consulta SQL para excluir o cliente
      connect.query(query, values, function (err, results) {
        if (err) {
          // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
          console.error(err);
          return res.status(500).json({ error: "Erro interno do servidor" });
        }

        // Verifica se algum cliente foi excluído
        if (results.affectedRows === 0) {
          // Retorna um erro se nenhum cliente for encontrado com o ID fornecido
          return res.status(404).json({ error: "Usuário não encontrado" });
        }

        // Retorna uma mensagem de sucesso se o cliente for excluído com sucesso
        return res.status(200).json({ message: "Usuário excluído com o telefone: " + userId });
      });
    } catch (error) {
      // Retorna um erro interno do servidor se ocorrer algum problema durante a execução da consulta
      console.error("Erro ao executar a consulta:", error);
      return res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};
