const connect = require("../db/connect");

module.exports = class FantasiaController {
    // Método para criar uma nova fantasia
    static async createFantasias(req, res) {
        const { nome, descricao, tamanho, preco, estoque_disponivel } = req.body;

        try {
            // Verifica se todos os campos obrigatórios estão preenchidos
            if (!nome || !descricao || !tamanho || !preco || !estoque_disponivel) {
                throw new Error("Por favor, preencha todos os campos corretamente");
            }

            // Monta a consulta SQL para inserir uma nova fantasia
            const query = `INSERT INTO fantasias (nome, descricao, tamanho, preco, estoque_disponivel) VALUES (?, ?, ?, ?, ?)`;
            const values = [nome, descricao, tamanho, preco, estoque_disponivel];

            // Executa a consulta SQL para inserir a nova fantasia
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao criar fantasia:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de sucesso se a fantasia for criada com sucesso
                return res.status(201).json({ message: "Fantasia criada com sucesso." });
            });
        } catch (error) {
            // Retorna um erro de requisição inválida se houver um problema durante a validação dos dados
            console.error("Erro ao criar fantasia:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    // Método para atualizar uma fantasia
    static async updateFantasias(req, res) {
        const productId = req.params.id;
        const { nome, descricao, preco, estoque_disponivel, tamanho } = req.body;

        try {
            // Verifica se o ID do produto é válido e se todos os campos obrigatórios estão presentes
            if (!productId || isNaN(productId) || parseInt(productId) <= 0 || !nome || !descricao || !preco || !estoque_disponivel || !tamanho) {
                throw new Error("ID de fantasia inválido ou campos incompletos");
            }

            // Query SQL para atualizar os campos na tabela fantasias
            const query = `UPDATE fantasias SET Nome = ?, Descricao = ?, Preco = ?, Estoque_disponivel = ?, Tamanho = ? WHERE ID_produto = ?`;
            const values = [nome, descricao, preco, estoque_disponivel, tamanho, productId];

            // Executa a consulta SQL para atualizar a fantasia
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao atualizar fantasia:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }

                // Verifica se houve algum registro atualizado
                if (results.affectedRows === 0) {
                    // Retorna um erro se a fantasia não for encontrada
                    return res.status(404).json({ message: "Fantasia não encontrada" });
                }

                // Retorna uma mensagem de sucesso
                return res.status(200).json({ message: "Fantasia atualizada" });
            });
        } catch (error) {
            // Retorna um erro de requisição inválida se houver um problema durante a validação dos dados
            console.error("Erro ao atualizar fantasia:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }

    // Método para obter todas as fantasias
    static async getFantasias(req, res) {
        try {
            // Monta a consulta SQL para obter todas as fantasias
            const query = `SELECT * FROM fantasias`;

            // Executa a consulta SQL para obter todas as fantasias
            connect.query(query, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao obter informações da fantasia:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Verifica se alguma fantasia foi encontrada
                if (results.length === 0) {
                    // Retorna um erro se nenhuma fantasia for encontrada
                    return res.status(404).json({ message: "Fantasia não encontrada" });
                }
                // Retorna os resultados se as fantasias forem encontradas com sucesso
                return res.status(200).json(results);
            });
        } catch (error) {
            // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
            console.error("Erro ao obter informações da fantasia:", error.message);
            return res.status(500).json({ message: "Erro interno do servidor" });
        }
    }

    // Método para obter uma fantasia por ID
    static async getFantasiasID(req, res) {
        const { id } = req.params;

        try {
            // Monta a consulta SQL para obter a fantasia com base no ID
            const query = `SELECT * FROM fantasias WHERE id_produto = ?`;
            const values = [id];

            // Executa a consulta SQL para obter a fantasia com base no ID
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao obter informações da fantasia:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Verifica se alguma fantasia foi encontrada
                if (results.length === 0) {
                    // Retorna um erro se nenhuma fantasia for encontrada
                    return res.status(404).json({ message: "Fantasia não encontrada" });
                }
                // Retorna os resultados se as fantasias forem encontradas com sucesso
                return res.status(200).json(results[0]);
            });
        } catch (error) {
            // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
            console.error("Erro ao obter informações da fantasia:", error.message);
            return res.status(500).json({ message: "Erro interno do servidor" });
        }
    }

    // Método para excluir uma fantasia por ID
    static async deleteFantasiaPorId(req, res) {
        const { id } = req.params;

        try {
            // Verifica se o ID da fantasia é válido
            if (!id || isNaN(id) || parseInt(id) <= 0) {
                throw new Error("ID de fantasia inválido");
            }

            // Monta a consulta SQL para excluir a fantasia com base no ID
            const query = `DELETE FROM fantasias WHERE id_produto = ?`;
            const values = [id];

            // Executa a consulta SQL para excluir a fantasia com base no ID
            connect.query(query, values, function (err, results) {
                if (err) {
                    // Retorna um erro interno do servidor se houver algum problema durante a execução da consulta
                    console.error("Erro ao excluir fantasia:", err);
                    return res.status(500).json({ message: "Erro interno do servidor" });
                }
                // Retorna uma mensagem de sucesso se a fantasia for excluída com sucesso
                return res.status(200).json({ message: `Fantasia removida com ID ${id}` });
            });
        } catch (error) {
            // Retorna um erro de requisição inválida se houver um problema durante a validação dos dados
            console.error("Erro ao excluir fantasia:", error.message);
            return res.status(400).json({ message: error.message });
        }
    }
};
