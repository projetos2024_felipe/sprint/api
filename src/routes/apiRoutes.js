const router = require('express').Router()
const clienteController = require('../controller/clienteController.js')
const dbController = require('../controller/dbController.js')
const reservaController = require('../controller/reservaController.js')
const acessoriosController = require('../controller/acessoriosController.js')
const fantasiasController = require('../controller/fantasiasController.js')
const enderecoController = require('../controller/enderecoController.js')

router.post('/postLogin', clienteController.postLogin);
router.post('/createCliente', clienteController.createCliente);
router.get('/getAllClientes', clienteController.getAllClientes);
router.get('/getAllClientesPorId/:id', clienteController.getClientesPorId);
router.put('/updateCliente/:id', clienteController.updateCliente);
router.delete('/deleteCliente/:id', clienteController.deleteCliente);

router.post('/createReserva', reservaController.createReserva);
router.get('/getAllReservas', reservaController.getAllReservas);
router.get('/getReservaPorId/:id', reservaController.getReservaPorId);
router.put('/updateReserva/:id', reservaController.updateReserva);
router.delete('/deleteReserva/:id', reservaController.deleteReserva);

router.get('/getAcessorios', acessoriosController.getAcessorios);
router.get('/getAcessorios/:id', acessoriosController.getAcessoriosID);
router.put('/updateAcessorios/:id', acessoriosController.updateAcessorios);
router.delete('/deleteAcessorios/:id', acessoriosController.deleteAcessoriosPorId);
router.post('/createAcessorio', acessoriosController.createAcessorio);

router.get('/getFantasias', fantasiasController.getFantasias);
router.get('/getFantasias/:id', fantasiasController.getFantasiasID);
router.put('/updateFantasias/:id', fantasiasController.updateFantasias);
router.delete('/deleteFantasias/:id', fantasiasController.deleteFantasiaPorId);
router.post('/createFantasias', fantasiasController.createFantasias);

router.get('/getEndereco', enderecoController.getEnderecos);
router.get('/getEnderecoID/:id', enderecoController.getEnderecoByID);
router.put('/updateEndereco/:id', enderecoController.updateEndereco);
router.delete('/deleteEndereco/:id', enderecoController.deleteEnderecoByID);
router.post('/createEndereco', enderecoController.createEndereco);

router.get('/getTables', dbController.getAllTableNames);
router.get('/getDesc', dbController.getTableDescriptions);

module.exports = router;